import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;


public class Quiz {
    public static Scanner scanner = new Scanner(System.in);
    public static Random random = new Random();


    private ArrayList<String> questionList;
    private ArrayList<ArrayList<String>> answerList;
    private ArrayList<String> correctAnswersList;
    private int[] costOfQuestionsArr;

    public Quiz(ArrayList<String> questionList, ArrayList<ArrayList<String>> answerList, ArrayList<String> correctAnswersList, int[] costOfQuestionsArr) {
        this.questionList = questionList;
        this.answerList = answerList;
        this.correctAnswersList = correctAnswersList;
        this.costOfQuestionsArr = costOfQuestionsArr;
    }


    public void generalTestOfMyLife() throws InterruptedException
    {

        int numberOfCorrectAnswers = 0;
        System.out.print("Enter the number of questions(for student): ");
        int numberOfStudentQuestions = scanner.nextInt();
        Instant start = Instant.now();
        int sum = 0;
        ArrayList<String> studentAnswers = new ArrayList<>();
        if (numberOfStudentQuestions == questionList.size())
        {
            for (int i = 0; i < numberOfStudentQuestions; i++)
            {

                System.out.println("Question №" + (i + 1));
                System.out.println(questionList.get(i));
                System.out.println("Answers: ");
                System.out.println();
                for(int k = 0; k < answerList.get(i).size(); k++)
                {
                    System.out.println(answerList.get(i).get(k));
                }
                String answer = scanner.next();
                studentAnswers.add(answer);
                if (correctAnswersList.get(i).equalsIgnoreCase(answer))
                {
                    numberOfCorrectAnswers++;

                    sum += costOfQuestionsArr[i];


                }
            }
        }


        if(numberOfStudentQuestions < questionList.size())
        {
            ArrayList<Integer> numbersForReduction = new ArrayList<>();

            for (int i = 0; i < numberOfStudentQuestions; i++)
            {
                int number = random.nextInt(numberOfStudentQuestions) + 1;
                if (!numbersForReduction.contains(number))
                {
                    numbersForReduction.add(number);
                }
            }
            for (int j = 0; j < numberOfStudentQuestions; j++)
            {
                System.out.println("Question №" + (j + 1));
                System.out.println(questionList.get(numbersForReduction.get(j)));
                System.out.println("Answers: ");
                for(int k = 0; k < answerList.get(j).size(); k++)
                {
                    System.out.println(answerList.get(j).get(k));
                }
                String answer = scanner.next();
                studentAnswers.add(answer);

                if (correctAnswersList.get(numbersForReduction.get(j)).equalsIgnoreCase(answer))
                {
                    numberOfCorrectAnswers++;
                    sum += costOfQuestionsArr[numbersForReduction.get(j)];


                }
            }
        }
        if(numberOfStudentQuestions > questionList.size())
        {
            System.out.println("Not today...");
            return;
        }
        for(int h = 0; h < numberOfStudentQuestions; h++)
        {
            System.out.println("In question " + (h + 1) + " correct answer is - " + correctAnswersList.get(h) + ", and you answered - " + studentAnswers.get(h));
        }

        Thread.sleep(10);
        Instant ends = Instant.now();
        System.out.println("You earn " + sum + " points");
        System.out.println("It's been " + Duration.between(start, ends).getSeconds() +  " seconds since the test started");

        System.out.println("You answered " + numberOfCorrectAnswers + " questions correctly.");



    }
}
