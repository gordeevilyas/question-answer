import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static Scanner scanner = new Scanner(System.in);



    public static void main(String[] args) throws InterruptedException
    {
        ArrayList<Quiz> tests = new ArrayList<>();
        tests.add(addQuestions());
        tests.get(0).generalTestOfMyLife();


    }


    public static Quiz addQuestions()
    {
        ArrayList<String> questions = new ArrayList<>();
        ArrayList<ArrayList<String>> answers = new ArrayList<>();
        ArrayList<String> correctAnswers = new ArrayList<>();


        System.out.print("Enter number of questions: ");
        int numberOfQuestions = Integer.parseInt(scanner.nextLine());
        int[] costOfQuestions = new int[numberOfQuestions];
        for(int i = 0; i < numberOfQuestions; i++)
        {
            System.out.print("Enter the question: ");
            questions.add(scanner.next());
            System.out.print("Enter cost of question: ");
            costOfQuestions[i] = scanner.nextInt();
            System.out.print("Enter number of answers: ");
            int numberOfAnswers = scanner.nextInt();
            ArrayList<String> answerList = new ArrayList<>();
            for(int j = 0; j < numberOfAnswers; j++)
            {
                System.out.print("Enter answer: ");
                answerList.add(scanner.next());

            }
            answers.add(answerList);
            System.out.print("Enter correct answer: ");
            correctAnswers.add(scanner.next());


        }

        Quiz quiz = new Quiz(questions, answers, correctAnswers, costOfQuestions);
        return quiz;
    }

}

